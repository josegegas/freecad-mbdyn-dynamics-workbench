# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################   set: [element, joint:3:rz ,3, joint, string="rz"];   set: [node,VARNAME,1,structural,string="XP[1]"];
import FreeCAD,FreeCADGui
import os
from plot import Plotnode, Plotjoint
 
__dir__ = os.path.dirname(__file__)

class PlotNodeDialog:
   objeto = None    
   expression = ""
    
   def __init__(self):
       self.form = FreeCADGui.PySideUic.loadUi( __dir__ + '/Dialogs/PlotNode.ui')
 
   def accept(self):       
       self.expression = self.form.lineEdit.text()  
       b = FreeCADGui.Selection.getSelection()
       node = int(b[0].label)
       FreeCAD.Console.PrintMessage("Plotting node results using expression: " + self.expression +"\n")                    
       FreeCADGui.Control.closeDialog()
       Plotnode(node, self.expression)

class PlotJointDialog:
   objeto = None    
   expression = ""
    
   def __init__(self):
       self.form = FreeCADGui.PySideUic.loadUi( __dir__ + '/Dialogs/PlotJoint.ui')
 
   def accept(self):       
       self.expression = self.form.lineEdit.text()  
       b = FreeCADGui.Selection.getSelection()
       joint = int(b[0].label)
       FreeCAD.Console.PrintMessage("Plotting node results using expression: " + self.expression +"\n")                    
       FreeCADGui.Control.closeDialog()
       Plotjoint(joint, self.expression)    

class PluginVariableDialogAxialRotation:
   objeto = None    
    
   def __init__(self, objeto):
       self.objeto = objeto
       self.form = FreeCADGui.PySideUic.loadUi( __dir__ + '/Dialogs/AxialRotation.ui')
 
   def accept(self):
       aux = "none"
       
       joint = FreeCADGui.Selection.getSelection()[0].label 
       
       Fx = self.form.Fx.isChecked()
       Fy = self.form.Fy.isChecked()
       Fz = self.form.Fz.isChecked()

       Mx = self.form.Mx.isChecked()
       My = self.form.My.isChecked()
       Mz = self.form.Mz.isChecked()
       
       rz = self.form.rz.isChecked()
       wz = self.form.wz.isChecked()

       if(Fx): aux = aux + '"Fx_' + joint + '": constraint reaction force in node 1 local direction 1 \n'
       if(Fy): aux = aux + '"Fy_' + joint + '": constraint reaction force in node 1 local direction 2 \n'
       if(Fz): aux = aux + '"Fz_' + joint + '": constraint reaction force in node 1 local direction 3 \n'
       if(Mx): aux = aux + '"Mx_' + joint + '": constraint reaction moment about node 1 local direction 1 \n'
       if(My): aux = aux + '"My_' + joint + '": constraint reaction moment about node 1 local direction 2 \n'
       if(Mz): aux = aux + '"Mz_' + joint + '": constraint reaction moment about node 1 local direction 3 \n'
       if(rz): aux = aux + '"rz_' + joint + '": relative rotation angle about revolute axis \n'
       if(wz): aux = aux + '"wz_' + joint + '": relative angular velocity about revolute axis \n'
       
       if aux != "none": aux = aux[4:-2]

       self.objeto.plugin_variables = aux 
       
       FreeCAD.Console.PrintMessage("Plugin variables updated: " + aux +"\n")                    
       
       FreeCADGui.Control.closeDialog()
        
class PluginVariableDialogClamp:
   objeto = None    
    
   def __init__(self, objeto):
       self.objeto = objeto
       self.form = FreeCADGui.PySideUic.loadUi( __dir__ + '/Dialogs/Clamp.ui')
 
   def accept(self):
       aux = "none"
       
       joint = FreeCADGui.Selection.getSelection()[0].label 
       
       Fx = self.form.Fx.isChecked()
       Fy = self.form.Fy.isChecked()
       Fz = self.form.Fz.isChecked()

       Mx = self.form.Mx.isChecked()
       My = self.form.My.isChecked()
       Mz = self.form.Mz.isChecked()
       
       if(Fx): aux = aux + '"Fx_' + joint + '": constraint reaction force in node 1 local direction 1 \n'
       if(Fy): aux = aux + '"Fy_' + joint + '": constraint reaction force in node 1 local direction 2 \n'
       if(Fz): aux = aux + '"Fz_' + joint + '": constraint reaction force in node 1 local direction 3 \n'
       if(Mx): aux = aux + '"Mx_' + joint + '": constraint reaction moment about node 1 local direction 1 \n'
       if(My): aux = aux + '"My_' + joint + '": constraint reaction moment about node 1 local direction 2 \n'
       if(Mz): aux = aux + '"Mz_' + joint + '": constraint reaction moment about node 1 local direction 3 \n'
       
       if aux != "none": aux = aux[4:-2]

       self.objeto.plugin_variables = aux 
       
       FreeCAD.Console.PrintMessage("Plugin variables updated: " + aux +"\n")                    
       
       FreeCADGui.Control.closeDialog()
       
class PluginVariableDialogNode:
   import FreeCADGui      
    
   objeto = None
   
   def __init__(self, objeto):
       self.objeto = objeto
       self.form = FreeCADGui.PySideUic.loadUi( __dir__ + '/Dialogs/StructuralDynamicNode.ui')
 
   def accept(self):
       node = FreeCADGui.Selection.getSelection()[0].label  
       
       aux = "none"
       
       X1 = self.form.X1.isChecked()      
       X2 = self.form.X2.isChecked()      
       X3 = self.form.X3.isChecked()      
       x1 = self.form.x1.isChecked()      
       x2 = self.form.x2.isChecked()      
       x3 = self.form.x3.isChecked()
       Phi1 = self.form.Phi1.isChecked()      
       Phi2 = self.form.Phi2.isChecked()      
       Phi3 = self.form.Phi3.isChecked()
       XP1 = self.form.XP1.isChecked()      
       XP2 = self.form.XP2.isChecked()      
       XP3 = self.form.XP3.isChecked()
       xP1 = self.form.xP1.isChecked()      
       xP2 = self.form.xP2.isChecked()      
       xP3 = self.form.xP3.isChecked()       
       Omega1 = self.form.Omega1.isChecked()
       Omega2 = self.form.Omega2.isChecked()
       Omega3 = self.form.Omega3.isChecked()
       omega1 = self.form.omega1.isChecked()
       omega2 = self.form.omega2.isChecked()
       omega3 = self.form.omega3.isChecked()      
       E1 = self.form.E1.isChecked()      
       E2 = self.form.E2.isChecked()      
       E3 = self.form.E3.isChecked() 
       E3131 = self.form.E3131.isChecked()      
       E3132 = self.form.E3132.isChecked()      
       E3133 = self.form.E3133.isChecked() 
       E3211 = self.form.E3211.isChecked()      
       E3212 = self.form.E3212.isChecked()      
       E3213 = self.form.E3213.isChecked() 
       PE0 = self.form.PE0.isChecked()      
       PE1 = self.form.PE1.isChecked()      
       PE2 = self.form.PE2.isChecked() 
       PE3 = self.form.PE3.isChecked()               
       
       if(X1): aux = aux + '"X_1_' + node + '": position in global direction 1 \n'
       if(X2): aux = aux + '"X_2_' + node + '": position in global direction 2 \n'
       if(X3): aux = aux + '"X_3_' + node + '": position in global direction 3 \n'
       if(x1): aux = aux + '"x_1_' + node + '": position in direction 1, in the reference frame of the node \n'  
       if(x2): aux = aux + '"x_2_' + node + '": position in direction 2, in the reference frame of the node \n'
       if(x3): aux = aux + '"x_3_' + node + '": position in direction 3, in the reference frame of the node \n'
       if(Phi1): aux = aux + '"Phi_1_' + node + '": orientation vector in global direction 1 \n'
       if(Phi2): aux = aux + '"Phi_2_' + node + '": orientation vector in global direction 2 \n'
       if(Phi3): aux = aux + '"Phi_3_' + node + '": orientation vector in global direction 3 \n'
       if(XP1): aux = aux + '"XP_1_' + node + '": velocity in global direction 1 \n'               
       if(XP2): aux = aux + '"XP_2_' + node + '": velocity in global direction 2 \n'
       if(XP3): aux = aux + '"XP_3_' + node + '": velocity in global direction 3 \n'
       if(xP1): aux = aux + '"xP_1_' + node + '": velocity in direction 1, in the reference frame of the node \n'          
       if(xP2): aux = aux + '"xP_2_' + node + '": velocity in direction 2, in the reference frame of the node \n'
       if(xP3): aux = aux + '"xP_3_' + node + '": velocity in direction 3, in the reference frame of the node \n'       
       if(Omega1): aux = aux + '"Omega_1_' + node + '": angular velocity in global direction 1 \n' 
       if(Omega2): aux = aux + '"Omega_2_' + node + '": angular velocity in global direction 2 \n' 
       if(Omega3): aux = aux + '"Omega_3_' + node + '": angular velocity in global direction 3 \n' 
       if(omega1): aux = aux + '"omega_1_' + node + '": angular velocity in direction 1, in the reference frame of the node \n' 
       if(omega2): aux = aux + '"omega_2_' + node + '": angular velocity in direction 2, in the reference frame of the node \n'
       if(omega3): aux = aux + '"omega_3_' + node + '": angular velocity in direction 3, in the reference frame of the node \n'
       if(E1): aux = aux + '"E_1_' + node + '": Cardan angle 1 (about global direction 1) \n'
       if(E2): aux = aux + '"E_2_' + node + '": Cardan angle 2 (about global direction 2) \n'
       if(E3): aux = aux + '"E_3_' + node + '": Cardan angle 3 (about global direction 3) \n'
       if(E3131): aux = aux + '"E313_1_' + node + '": Cardan angle 1 (about global direction 3) \n'
       if(E3132): aux = aux + '"E313_2_' + node + '": Cardan angle 2 (about global direction 1) \n'
       if(E3133): aux = aux + '"E313_3_' + node + '": Cardan angle 3 (about global direction 3) \n'
       if(E3211): aux = aux + '"E321_1_' + node + '": Cardan angle 1 (about global direction 3) \n'
       if(E3212): aux = aux + '"E321_2_' + node + '": Cardan angle 2 (about global direction 2) \n'
       if(E3213): aux = aux + '"E321_3_' + node + '": Cardan angle 3 (about global direction 1) \n'
       if(PE0): aux = aux + '"PE_0_' + node + '": Euler parameter 0 \n'
       if(PE1): aux = aux + '"PE_1_' + node + '": Euler parameter 1 \n'
       if(PE2): aux = aux + '"PE_2_' + node + '": Euler parameter 2 \n'
       if(PE3): aux = aux + '"PE_3_' + node + '": Euler parameter 3 \n'
       
       
       if aux != "none": aux = aux[4:-2]

       self.objeto.plugin_variables = aux 
       
       FreeCAD.Console.PrintMessage("Static node plugin variables updated: " + aux +"\n")                    
       
       FreeCADGui.Control.closeDialog()             
       
class PluginVariableDialogDynamicNode:
   import FreeCADGui      
    
   objeto = None
   
   def __init__(self, objeto):
       self.objeto = objeto
       self.form = FreeCADGui.PySideUic.loadUi( __dir__ + '/Dialogs/StructuralDynamicNode.ui')
 
   def accept(self):
       node = FreeCADGui.Selection.getSelection()[0].label  
       
       aux = "none"
       
       X1 = self.form.X1.isChecked()      
       X2 = self.form.X2.isChecked()      
       X3 = self.form.X3.isChecked()      
       x1 = self.form.x1.isChecked()      
       x2 = self.form.x2.isChecked()      
       x3 = self.form.x3.isChecked()
       Phi1 = self.form.Phi1.isChecked()      
       Phi2 = self.form.Phi2.isChecked()      
       Phi3 = self.form.Phi3.isChecked()
       XP1 = self.form.XP1.isChecked()      
       XP2 = self.form.XP2.isChecked()      
       XP3 = self.form.XP3.isChecked()
       xP1 = self.form.xP1.isChecked()      
       xP2 = self.form.xP2.isChecked()      
       xP3 = self.form.xP3.isChecked()       
       Omega1 = self.form.Omega1.isChecked()
       Omega2 = self.form.Omega2.isChecked()
       Omega3 = self.form.Omega3.isChecked()
       omega1 = self.form.omega1.isChecked()
       omega2 = self.form.omega2.isChecked()
       omega3 = self.form.omega3.isChecked()      
       E1 = self.form.E1.isChecked()      
       E2 = self.form.E2.isChecked()      
       E3 = self.form.E3.isChecked() 
       E3131 = self.form.E3131.isChecked()      
       E3132 = self.form.E3132.isChecked()      
       E3133 = self.form.E3133.isChecked() 
       E3211 = self.form.E3211.isChecked()      
       E3212 = self.form.E3212.isChecked()      
       E3213 = self.form.E3213.isChecked() 
       PE0 = self.form.PE0.isChecked()      
       PE1 = self.form.PE1.isChecked()      
       PE2 = self.form.PE2.isChecked() 
       PE3 = self.form.PE3.isChecked()        
       XPP1 = self.form.XPP1.isChecked()      
       XPP2 = self.form.XPP2.isChecked()      
       XPP3 = self.form.XPP3.isChecked()
       OmegaP1 = self.form.OmegaP1.isChecked()
       OmegaP2 = self.form.OmegaP2.isChecked()
       OmegaP3 = self.form.OmegaP3.isChecked()
       omegaP1 = self.form.omegaP1.isChecked()
       omegaP2 = self.form.omegaP2.isChecked()
       omegaP3 = self.form.omegaP3.isChecked()      
     
       
       if(X1): aux = aux + '"X_1_' + node + '": position in global direction 1 \n'
       if(X2): aux = aux + '"X_2_' + node + '": position in global direction 2 \n'
       if(X3): aux = aux + '"X_3_' + node + '": position in global direction 3 \n'
       if(x1): aux = aux + '"x_1_' + node + '": position in direction 1, in the reference frame of the node \n'  
       if(x2): aux = aux + '"x_2_' + node + '": position in direction 2, in the reference frame of the node \n'
       if(x3): aux = aux + '"x_3_' + node + '": position in direction 3, in the reference frame of the node \n'
       if(Phi1): aux = aux + '"Phi_1_' + node + '": orientation vector in global direction 1 \n'
       if(Phi2): aux = aux + '"Phi_2_' + node + '": orientation vector in global direction 2 \n'
       if(Phi3): aux = aux + '"Phi_3_' + node + '": orientation vector in global direction 3 \n'
       if(XP1): aux = aux + '"XP_1_' + node + '": velocity in global direction 1 \n'               
       if(XP2): aux = aux + '"XP_2_' + node + '": velocity in global direction 2 \n'
       if(XP3): aux = aux + '"XP_3_' + node + '": velocity in global direction 3 \n'
       if(xP1): aux = aux + '"xP_1_' + node + '": velocity in direction 1, in the reference frame of the node \n'          
       if(xP2): aux = aux + '"xP_2_' + node + '": velocity in direction 2, in the reference frame of the node \n'
       if(xP3): aux = aux + '"xP_3_' + node + '": velocity in direction 3, in the reference frame of the node \n'       
       if(Omega1): aux = aux + '"Omega_1_' + node + '": angular velocity in global direction 1 \n' 
       if(Omega2): aux = aux + '"Omega_2_' + node + '": angular velocity in global direction 2 \n' 
       if(Omega3): aux = aux + '"Omega_3_' + node + '": angular velocity in global direction 3 \n' 
       if(omega1): aux = aux + '"omega_1_' + node + '": angular velocity in direction 1, in the reference frame of the node \n' 
       if(omega2): aux = aux + '"omega_2_' + node + '": angular velocity in direction 2, in the reference frame of the node \n'
       if(omega3): aux = aux + '"omega_3_' + node + '": angular velocity in direction 3, in the reference frame of the node \n'
       if(E1): aux = aux + '"E_1_' + node + '": Cardan angle 1 (about global direction 1) \n'
       if(E2): aux = aux + '"E_2_' + node + '": Cardan angle 2 (about global direction 2) \n'
       if(E3): aux = aux + '"E_3_' + node + '": Cardan angle 3 (about global direction 3) \n'
       if(E3131): aux = aux + '"E313_1_' + node + '": Cardan angle 1 (about global direction 3) \n'
       if(E3132): aux = aux + '"E313_2_' + node + '": Cardan angle 2 (about global direction 1) \n'
       if(E3133): aux = aux + '"E313_3_' + node + '": Cardan angle 3 (about global direction 3) \n'
       if(E3211): aux = aux + '"E321_1_' + node + '": Cardan angle 1 (about global direction 3) \n'
       if(E3212): aux = aux + '"E321_2_' + node + '": Cardan angle 2 (about global direction 2) \n'
       if(E3213): aux = aux + '"E321_3_' + node + '": Cardan angle 3 (about global direction 1) \n'
       if(PE0): aux = aux + '"PE_0_' + node + '": Euler parameter 0 \n'
       if(PE1): aux = aux + '"PE_1_' + node + '": Euler parameter 1 \n'
       if(PE2): aux = aux + '"PE_2_' + node + '": Euler parameter 2 \n'
       if(PE3): aux = aux + '"PE_3_' + node + '": Euler parameter 3 \n'
       if(XPP1): aux = aux + '"XPP_1_' + node + '": acceleration in global direction 1 \n'
       if(XPP2): aux = aux + '"XPP_2_' + node + '": acceleration in global direction 2 \n'
       if(XPP3): aux = aux + '"XPP_3_' + node + '": acceleration in global direction 3 \n'
       if(XPP1): aux = aux + '"xPP_1_' + node + '": acceleration in direction 1, in the reference frame of the node \n'
       if(XPP2): aux = aux + '"xPP_2_' + node + '": acceleration in direction 2, in the reference frame of the node \n'
       if(XPP3): aux = aux + '"xPP_3_' + node + '": acceleration in direction 3, in the reference frame of the node \n'       
       if(OmegaP1): aux = aux + '"OmegaP_1_' + node + '": angular acceleration in global direction 1 \n'   
       if(OmegaP2): aux = aux + '"OmegaP_2_' + node + '": angular acceleration in global direction 2 \n'   
       if(OmegaP3): aux = aux + '"OmegaP_3_' + node + '": angular acceleration in global direction 3 \n'   
       if(omegaP1): aux = aux + '"omegaP_1_' + node + '": angular acceleration in direction 1, in the reference frame of the node \n'
       if(omegaP2): aux = aux + '"omegaP_2_' + node + '": angular acceleration in direction 2, in the reference frame of the node \n'
       if(omegaP3): aux = aux + '"omegaP_3_' + node + '": angular acceleration in direction 3, in the reference frame of the node \n'       
       
       if aux != "none": aux = aux[4:-2]

       self.objeto.plugin_variables = aux 
       
       FreeCAD.Console.PrintMessage("Dynamic node plugin variables updated: " + aux +"\n")                    
       
       FreeCADGui.Control.closeDialog()       