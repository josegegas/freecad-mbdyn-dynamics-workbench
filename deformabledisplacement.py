# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################
'''
     joint: 2, # joint label
            deformable displacement joint,
            1, # node 1
            null, # relative offset to 1 [m]
            2, # node 2
            null, # relative offset to 2 [m]
            linear elastic isotropic,
            1., # spring stiffness in Newtons per meter
            prestrain, single, 1., 1., 0., const, 0.0282842712474619; #direction of oscillation and spring natural lenght in meters
'''

from sympy import Point3D, Line3D
import FreeCAD
import Draft

class DeformableDisplacement:                  
    def __init__(self, obj, label, node1, node2, referenceObject1, referenceObject2, reference1, reference2):        
        
        x = node1.absolute_position_X
        y = node1.absolute_position_Y
        z = node1.absolute_position_Z          
       

        #Natural lenght is the distance between the two nodes:       
        aux = pow(pow((node2.absolute_position_X - node1.absolute_position_X),2) + pow((node2.absolute_position_Y - node1.absolute_position_Y),2) + pow((node2.absolute_position_Z - node1.absolute_position_Z),2),(0.5)) 
        lenght = FreeCAD.Units.Quantity(aux,FreeCAD.Units.Unit('mm')) 
        
        obj.addExtension("App::GroupExtensionPython")        
        
        obj.addProperty("App::PropertyString","joint","Deformable displacement joint","joint",1).joint = "deformable displacement"    
        obj.addProperty("App::PropertyString","label","Deformable displacement joint","label",1).label = str(label)
        obj.addProperty("App::PropertyString","node_1","Deformable displacement joint","node_1",1).node_1 = str(node1.label)
        obj.addProperty("App::PropertyString","node_2","Deformable displacement joint","node_2",1).node_2 = str(node2.label)
        obj.addProperty("App::PropertyString","constitutive law","Deformable displacement joint","constitutive law").constitutive_law = "type here the label of a constitutive law"
        obj.addProperty("App::PropertyString","direction","Deformable displacement joint","direction",1).direction = ""
        obj.addProperty("App::PropertyDistance","natural lenght","Deformable displacement joint","natural lenght",1).natural_lenght = lenght
        
        obj.addProperty("App::PropertyDistance","absolute_pin_position_X","Absolute pin position","absolute_pin_position_X",1).absolute_pin_position_X = x
        obj.addProperty("App::PropertyDistance","absolute_pin_position_Y","Absolute pin position","absolute_pin_position_Y",1).absolute_pin_position_Y = y
        obj.addProperty("App::PropertyDistance","absolute_pin_position_Z","Absolute pin position","absolute_pin_position_Z",1).absolute_pin_position_Z = z

        #Relative offset 1:          
        obj.addProperty("App::PropertyDistance","relative offset 1 X","Relative offset 1","relative offset 1 X",1).relative_offset_1_X = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('mm'))
        obj.addProperty("App::PropertyDistance","relative offset 1 Y","Relative offset 1","relative offset 1 Y",1).relative_offset_1_Y = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('mm'))
        obj.addProperty("App::PropertyDistance","relative offset 1 Z","Relative offset 1","relative offset 1 Z",1).relative_offset_1_Z = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('mm'))
        
        #Relative offset 2: 
        obj.addProperty("App::PropertyDistance","relative offset 2 X","Relative offset 2","relative offset 2 X",1).relative_offset_2_X = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('mm'))
        obj.addProperty("App::PropertyDistance","relative offset 2 Y","Relative offset 2","relative offset 2 Y",1).relative_offset_2_Y = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('mm'))
        obj.addProperty("App::PropertyDistance","relative offset 2 Z","Relative offset 2","relative offset 2 Z",1).relative_offset_2_Z = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('mm'))

        
        obj.addProperty("App::PropertyFloat","force vector multiplier","Animation","force vector multiplier").force_vector_multiplier = 1.0
        obj.addProperty("App::PropertyString","frame","Animation","frame").frame = 'local'
        obj.addProperty("App::PropertyString","animate","Animation","animate").animate = 'false'
        
        #The references define the position and orientation of the joint:                
        obj.addProperty("App::PropertyLinkSub","reference_1","Reference 1","reference_1")
        obj.addProperty("App::PropertyEnumeration","attachment_mode_1","Reference 1","attachment mode 1")
        obj.attachment_mode_1 = ['geometry´s center of mass', 'body´s center of mass', 'arc´s center']
        
        obj.addProperty("App::PropertyLinkSub","reference_2","Reference 2","reference_2")        
        obj.addProperty("App::PropertyEnumeration","attachment_mode_2","Reference 2","attachment mode 2")
        obj.attachment_mode_2 = ['geometry´s center of mass', 'body´s center of mass', 'arc´s center']      

        if referenceObject1 == referenceObject2:
            obj.reference_1 = (referenceObject1, reference1.SubElementNames[0])
            obj.reference_2 = (referenceObject2, reference2.SubElementNames[1])            
        
        else:
            obj.reference_1 = (referenceObject1, reference1.SubElementNames[0])
            obj.reference_2 = (referenceObject2, reference2.SubElementNames[0]) 

        obj.Proxy = self
         
        #Add joint´s rotation axis. This axis determines the "absolute_pin_orientation_matrix" and the jont´s position:                    
        p1 = obj.reference_1[0].Shape.getElement(obj.reference_1[1][0]).CenterOfMass
        p2 = obj.reference_2[0].Shape.getElement(obj.reference_2[1][0]).CenterOfMass
        Llength = FreeCAD.Units.Quantity(FreeCAD.ActiveDocument.getObjectsByLabel("X")[0].End[0]/4,FreeCAD.Units.Unit('mm'))
        
        #Create the rotation axis:
        l = Draft.makeLine(p1, p2)
        l.Label = 'z: joint: '+ str(label)
        l.ViewObject.LineColor = (0.00,0.00,1.00)
        l.ViewObject.PointColor = (0.00,0.00,1.00)
        l.ViewObject.DrawStyle = u"Dashed"   
        l.ViewObject.LineWidth = 1.00
        l.ViewObject.PointSize = 1.00
        l.ViewObject.EndArrow = True
        l.ViewObject.ArrowType = u"Arrow"
        l.ViewObject.ArrowSize = str(Llength/100)#+' mm'
                     
        #Add the vector to visualize reaction forces        
        p1 = FreeCAD.Vector((p1.x+p2.x)/2 , (p1.y+p2.y)/2, (p1.z+p2.z)/2)
        p2 = FreeCAD.Vector( p1.x+Llength.Value, p1.y+Llength.Value, p1.z+Llength.Value)  
        
        d = Draft.makeLine(p1, p2)
        d.ViewObject.LineColor = (1.00,0.00,0.00)
        d.ViewObject.PointColor = (1.00,0.00,0.00)  
        d.ViewObject.LineWidth = 1.00
        d.ViewObject.PointSize = 1.00
        d.ViewObject.EndArrow = True
        d.ViewObject.ArrowType = u"Arrow"
        d.ViewObject.ArrowSize = str(Llength/75)#+' mm'
        d.Label = "jf: "+ str(label)  
        
    def execute(self, fp):
            ZZ = FreeCAD.ActiveDocument.getObjectsByLabel("z: joint: "+fp.label)[0]#get the joint´s line
            JF = FreeCAD.ActiveDocument.getObjectsByLabel("jf: "+fp.label)[0]
            Llength = FreeCAD.Units.Quantity(FreeCAD.ActiveDocument.getObjectsByLabel("X")[0].End[0]/4,FreeCAD.Units.Unit('mm'))
            
            if fp.attachment_mode_1 == 'geometry´s center of mass':
                p1 = fp.reference_1[0].Shape.getElement(fp.reference_1[1][0]).CenterOfGravity
                
            if fp.attachment_mode_2 == 'geometry´s center of mass':
                p2 = fp.reference_2[0].Shape.getElement(fp.reference_2[1][0]).CenterOfGravity
            
            if fp.attachment_mode_1 == 'body´s center of mass':
                p1 = fp.reference_1[0].Shape.CenterOfGravity
                
            if fp.attachment_mode_2 == 'body´s center of mass':    
                p2 = fp.reference_2[0].Shape.CenterOfGravity
                
            if fp.attachment_mode_1 == 'arc´s center':
                p1 = fp.reference_1[0].Shape.getElement(fp.reference_1[1][0]).Curve.Center
                
            if fp.attachment_mode_2 == 'arc´s center':
                p2 = fp.reference_2[0].Shape.getElement(fp.reference_2[1][0]).Curve.Center

            ZZ.Start = p1
            ZZ.End = p2
            
            node1 = FreeCAD.ActiveDocument.getObjectsByLabel("structural: " + fp.node_1)[0]
            x = node1.absolute_position_X
            y = node1.absolute_position_Y
            z = node1.absolute_position_Z
            
            JF.Start =  FreeCAD.Vector(x, y, z)
            JF.End = FreeCAD.Vector(x+Llength, y+Llength, z+Llength)             

            fp.absolute_pin_position_X = x
            fp.absolute_pin_position_Y = y
            fp.absolute_pin_position_Z = z 

            #Two 3D points that define the joint´s line:
            p1, p2 = Point3D(ZZ.Start[0], ZZ.Start[1], ZZ.Start[2]), Point3D(ZZ.End[0], ZZ.End[1], ZZ.End[2]) 
            #A line that defines the joint´s orientation:
            l1 = Line3D(p1, p2)#Line that defines the joint

            #generate the orientation matrix (it has to be a unit vector, so that it does not alter the spring natural leght):
            magnitude = (l1.direction[0]**2+l1.direction[1]**2+l1.direction[2]**2)**0.5#Calculate the vector´s magnitude
            
            fp.direction = str(l1.direction[0]/magnitude) +", "+ str(l1.direction[1]/magnitude) + ", " + str(l1.direction[2]/magnitude)
            
            FreeCAD.Console.PrintMessage("DEFORMABLE DISPLACEMENT JOINT: " +fp.label+" successful recomputation...\n") 
             


             
