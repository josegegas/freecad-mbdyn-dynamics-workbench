# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
################################################################################### 
'''
This class receives a non-parametric body and creates a structural dynamic node positioned at the body's center of mass and parallel to the global refernce frame.
A structural node is an owner of kinematic degrees of freedom. 
It can assume the six degrees of freedom, three for possition and three for orientation.

The syntax is:

structural: <label>, 
            dynamic, 
            <position>, 
            <orientation>,
            <velocity>, 
            <angular velocity> ;
        
Where:
<label> is an integer number that identifies the node. Label the 3D body in FreeCAD with a unique integer number to identify it's node, before you call this class.
<position> is the node's possition relative to the absolute reference frame. This is equal to the body's center of mass position.
<orientation> is the node's orientation relative to the absolute reference frame. I assume all bodies start at: euler, 0., 0., 0.
<velocity> is the initial nodes's velocity. The user can define this from FreeCAD
<angular velocity> the initial angular velocity. To be defined by the user too.
'''

import FreeCAD
import Draft
from sympy import Point3D, Line3D
#Note: this object´s properties are rounded while writing the input file, in writeinpfile.py script
class Structuraldynamicnode: 
    def __init__(self, obj, baseBody, referenceObject1, reference1): 
                
        #Give the object the ability to contain other objetcs:
        obj.addExtension("App::GroupExtensionPython")        
        
        #Give the object its properties:
        obj.addProperty("App::PropertyString","label","Structural dynamic node","A unique numerical label that identifies this node is automatically assigned",1).label = baseBody.label    
        obj.addProperty("App::PropertyString","type","Structural dynamic node","The type of the node is: dynamic",1).type = 'dynamic'

        obj.addProperty("App::PropertyString","plugin_variables","List of plugin variables","The list of plugin variables connected to this node is automatically updated when plugin variables are created",1).plugin_variables = ""
        
        obj.addProperty("App::PropertyString","joints","List of joints","The list of joints asociated to this node is automatically updated when joints are applied",1).joints = ""        
        
        obj.addProperty("App::PropertyDistance","absolute_position_X","Initial conditions: absolute position",'To access this variable type "Px_" followed by the number of node, for instance "Px_1".',1)
        obj.addProperty("App::PropertyDistance","absolute_position_Y","Initial conditions: absolute position",'To access this variable type "Px_" followed by the number of node, for instance "Py_1".',1)
        obj.addProperty("App::PropertyDistance","absolute_position_Z","Initial conditions: absolute position",'To access this variable type "Px_" followed by the number of node, for instance "Pz_1".',1)
        
        obj.addProperty("App::PropertyAngle","yaw","Initial conditions: absolute orientation","Initial absolute orientation, yaw")
        obj.addProperty("App::PropertyAngle","pitch","Initial conditions: absolute orientation","Initial absolute orientation, pitch")
        obj.addProperty("App::PropertyAngle","roll","Initial conditions: absolute orientation","Initial absolute orientation, roll")
        obj.addProperty("App::PropertyString","absolute_orientation_matrix","Initial conditions: absolute orientation","The absolute orientation matrix is automatically computed from the angles below",1).absolute_orientation_matrix = "3, 0., 0., 1., 2, 0, 1., 0."  
                
        obj.addProperty("App::PropertySpeed","absolute_velocity_X","Initial conditions: absolute velocity",'To access this variable type "Vx_" followed by the number of node, for instance "Vx_1".')
        obj.addProperty("App::PropertySpeed","absolute_velocity_Y","Initial conditions: absolute velocity",'To access this variable type "Vy_" followed by the number of node, for instance "Vy_1".')
        obj.addProperty("App::PropertySpeed","absolute_velocity_Z","Initial conditions: absolute velocity",'To access this variable type "Vz_" followed by the number of node, for instance "Vz_1".')
        
        obj.addProperty("App::PropertyQuantity","absolute_angular_velocity_X","Initial conditions: absolute angular velocity",'To access this variable type "Wx_" followed by the number of node, for instance "Wx_1".')
        obj.absolute_angular_velocity_X = FreeCAD.Units.Unit('deg/s') 
        obj.addProperty("App::PropertyQuantity","absolute_angular_velocity_Y","Initial conditions: absolute angular velocity",'To access this variable type "Wy_" followed by the number of node, for instance "Wy_1".')
        obj.absolute_angular_velocity_Y = FreeCAD.Units.Unit('deg/s') 
        obj.addProperty("App::PropertyQuantity","absolute_angular_velocity_Z","Initial conditions: absolute angular velocity",'To access this variable type "Wz_" followed by the number of node, for instance "Wz_1".')
        obj.absolute_angular_velocity_Z = FreeCAD.Units.Unit('deg/s') 
        
        obj.addProperty("App::PropertyLinkSub","reference","Attachment","reference")

        try:
            obj.reference = (referenceObject1, reference1.SubElementNames[0])
        except:
            obj.reference = (referenceObject1, reference1)

        obj.addProperty("App::PropertyEnumeration","attachment_mode","Attachment","attachment mode")
        obj.attachment_mode=['body´s center of mass', 'geometry´s center of mass', 'arc´s center', 'start point', 'end point']
        
        obj.Proxy = self
                     
        #Add the coordinate system and an system to the GUI. The coordinate system represents the position of the node in space:
        length = (baseBody.Shape.BoundBox.XLength+baseBody.Shape.BoundBox.YLength+baseBody.Shape.BoundBox.ZLength)/6 # Calculate the body characteristic length. Will be used to size the arrows that represent the node.
        p1 = FreeCAD.Vector(0, 0, 0)
        poss = obj.reference[0].Shape.getElement(obj.reference[1][0]).CenterOfMass
        
        #Add x vector of the coordinate system:
        p2 = FreeCAD.Vector(length, 0, 0)
        l = Draft.makeLine(p1, p2) 
        l.Label = 'x: structural: '+ baseBody.label          
        l.ViewObject.LineColor = (1.00,0.00,0.00)
        l.ViewObject.PointColor = (1.00,0.00,0.00)
        l.Placement=FreeCAD.Placement(poss, FreeCAD.Rotation(FreeCAD.Vector(0,0,1),0), FreeCAD.Vector(0,0,0))
        l.ViewObject.EndArrow = True
        l.ViewObject.ArrowType = u"Arrow"
        l.ViewObject.LineWidth = 1.00
        l.ViewObject.PointSize = 1.00
        l.ViewObject.ArrowSize = str(length/15)+' mm'
        #Add y vector of the coordinate system:
        p2 = FreeCAD.Vector(0, length, 0)
        l = Draft.makeLine(p1, p2)        
        l.Label = 'y: structural: '+ baseBody.label        
        l.ViewObject.LineColor = (0.00,1.00,0.00)
        l.ViewObject.PointColor = (0.00,1.00,0.00)
        l.Placement=FreeCAD.Placement(poss, FreeCAD.Rotation(FreeCAD.Vector(0,0,1),0), FreeCAD.Vector(0,0,0))
        l.ViewObject.EndArrow = True
        l.ViewObject.ArrowType = u"Arrow"
        l.ViewObject.LineWidth = 1.00
        l.ViewObject.PointSize = 1.00
        l.ViewObject.ArrowSize = str(length/15)+' mm'  
        #Add z vector of the coordinate system:
        p2 = FreeCAD.Vector(0, 0, 0+length)
        l = Draft.makeLine(p1, p2)        
        l.Label = 'z: structural: '+ baseBody.label                        
        l.ViewObject.LineColor = (0.00,0.00,1.00)
        l.ViewObject.PointColor = (0.00,0.00,1.00)
        l.Placement=FreeCAD.Placement(poss, FreeCAD.Rotation(FreeCAD.Vector(0,0,1),0), FreeCAD.Vector(0,0,0))
        l.ViewObject.EndArrow = True  
        l.ViewObject.ArrowType = u"Arrow"
        l.ViewObject.LineWidth = 1.00
        l.ViewObject.PointSize = 1.00
        l.ViewObject.ArrowSize = str(length/15)+' mm'                                
    
                   
    def onChanged(self, fp, prop):        
        try: 
            if str(prop)=="yaw" or str(prop)=="pitch" or str(prop)=="roll":
                #Get the new position:
                x = fp.absolute_position_X
                y = fp.absolute_position_Y
                z = fp.absolute_position_Z
                #Get the new orientation:
                yaw = fp.yaw
                pitch = fp.pitch
                roll = fp.roll
                #Move the arrows and text                
                FreeCAD.ActiveDocument.getObjectsByLabel("x: structural: "+fp.label)[0].Placement=FreeCAD.Placement(FreeCAD.Vector(x,y,z), FreeCAD.Rotation(yaw,pitch,roll), FreeCAD.Vector(0,0,0))
                FreeCAD.ActiveDocument.getObjectsByLabel("y: structural: "+fp.label)[0].Placement=FreeCAD.Placement(FreeCAD.Vector(x,y,z), FreeCAD.Rotation(yaw,pitch,roll), FreeCAD.Vector(0,0,0))
                FreeCAD.ActiveDocument.getObjectsByLabel("z: structural: "+fp.label)[0].Placement=FreeCAD.Placement(FreeCAD.Vector(x,y,z), FreeCAD.Rotation(yaw,pitch,roll), FreeCAD.Vector(0,0,0))
                #Update the view:
                FreeCAD.ActiveDocument.getObjectsByLabel("structural: "+fp.label)[0].Visibility = False
                FreeCAD.ActiveDocument.getObjectsByLabel("structural: "+fp.label)[0].Visibility = True
                
                xx = FreeCAD.ActiveDocument.getObjectsByLabel("z: structural: "+fp.label)[0]
                p1, p2 = Point3D(xx.Start[0], xx.Start[1], xx.Start[2]), Point3D(xx.End[0], xx.End[1], xx.End[2]) 
                l1 = Line3D(p1, p2)
                
                magnitude = (l1.direction[0]**2+l1.direction[1]**2+l1.direction[2]**2)**0.5#Calculate the vector´s magnitude
                
                zz = FreeCAD.ActiveDocument.getObjectsByLabel("y: structural: "+fp.label)[0]
                p11, p22 = Point3D(zz.Start[0], zz.Start[1], zz.Start[2]), Point3D(zz.End[0], zz.End[1], zz.End[2]) 
                l2 = Line3D(p11, p22)#Line that defines the joint            
                magnitude1 = (l1.direction[0]**2+l1.direction[1]**2+l1.direction[2]**2)**0.5#Calculate the vector´s magnitude
                
                fp.absolute_orientation_matrix = "3, "+ str(l1.direction[0]/magnitude) +", "+ str(l1.direction[1]/magnitude) + ", " + str(l1.direction[2]/magnitude) + ", 2, "+ str(l2.direction[0]/magnitude1) +", "+ str(l2.direction[1]/magnitude1) + ", " + str(l2.direction[2]/magnitude)
                    
        except:
            pass
            
    def execute(self, fp):        
        if fp.attachment_mode == 'geometry´s center of mass':
            x = fp.reference[0].Shape.getElement(fp.reference[1][0]).CenterOfGravity.x
            y = fp.reference[0].Shape.getElement(fp.reference[1][0]).CenterOfGravity.y
            z = fp.reference[0].Shape.getElement(fp.reference[1][0]).CenterOfGravity.z
        
        if fp.attachment_mode == 'body´s center of mass':
            x = fp.reference[0].Shape.CenterOfGravity.x
            y = fp.reference[0].Shape.CenterOfGravity.y
            z = fp.reference[0].Shape.CenterOfGravity.z
            
        if fp.attachment_mode == 'arc´s center':
            x = fp.reference[0].Shape.getElement(fp.reference[1][0]).Curve.Center.x
            y = fp.reference[0].Shape.getElement(fp.reference[1][0]).Curve.Center.y
            z = fp.reference[0].Shape.getElement(fp.reference[1][0]).Curve.Center.z
            
        if fp.attachment_mode == 'start point':
            x = fp.reference[0].Shape.getElement(fp.reference[1][0]).toNurbs().Edges[0].Curve.StartPoint[0]
            y = fp.reference[0].Shape.getElement(fp.reference[1][0]).toNurbs().Edges[0].Curve.StartPoint[1]
            z = fp.reference[0].Shape.getElement(fp.reference[1][0]).toNurbs().Edges[0].Curve.StartPoint[2]

        if fp.attachment_mode == 'end point':
            x = fp.reference[0].Shape.getElement(fp.reference[1][0]).toNurbs().Edges[0].Curve.EndPoint[0]
            y = fp.reference[0].Shape.getElement(fp.reference[1][0]).toNurbs().Edges[0].Curve.EndPoint[1]
            z = fp.reference[0].Shape.getElement(fp.reference[1][0]).toNurbs().Edges[0].Curve.EndPoint[2]
        
        fp.absolute_position_X = x
        fp.absolute_position_Y = y
        fp.absolute_position_Z = z

        FreeCAD.ActiveDocument.getObjectsByLabel("x: structural: "+fp.label)[0].Placement=FreeCAD.Placement(FreeCAD.Vector(x,y,z), FreeCAD.Rotation(fp.yaw,fp.pitch,fp.roll), FreeCAD.Vector(0,0,0))
        FreeCAD.ActiveDocument.getObjectsByLabel("y: structural: "+fp.label)[0].Placement=FreeCAD.Placement(FreeCAD.Vector(x,y,z), FreeCAD.Rotation(fp.yaw,fp.pitch,fp.roll), FreeCAD.Vector(0,0,0))
        FreeCAD.ActiveDocument.getObjectsByLabel("z: structural: "+fp.label)[0].Placement=FreeCAD.Placement(FreeCAD.Vector(x,y,z), FreeCAD.Rotation(fp.yaw,fp.pitch,fp.roll), FreeCAD.Vector(0,0,0))
        
        FreeCAD.Console.PrintMessage("DYNAMIC NODE: " +fp.label+ " successful recomputation...\n")
        
        
    def onDocumentRestored(self, fp):
        fp.absolute_angular_velocity_X = FreeCAD.Units.Unit('deg/s')     
        fp.absolute_angular_velocity_Y = FreeCAD.Units.Unit('deg/s')     
        fp.absolute_angular_velocity_Z = FreeCAD.Units.Unit('deg/s')     
        